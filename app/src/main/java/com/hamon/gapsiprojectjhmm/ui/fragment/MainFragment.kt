package com.hamon.gapsiprojectjhmm.ui.fragment

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import com.hamon.gapsiprojectjhmm.databinding.FragmentMainBinding
import com.hamon.gapsiprojectjhmm.ui.adapter.ItemAdapter
import com.hamon.gapsiprojectjhmm.ui.adapter.QueryAdapter
import com.hamon.gapsiprojectjhmm.viewmodel.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    private val binding: FragmentMainBinding by lazy {
        FragmentMainBinding.inflate(LayoutInflater.from(context), null, false)
    }
    private val viewModel: MainViewModel by viewModel()
    private val itemAdapter: ItemAdapter by lazy {
        ItemAdapter()
    }
    private val queryAdapter: QueryAdapter by lazy {
        QueryAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        setupOnClickListener()
        setupObservable()
        setupTextWatcher()
    }

    private fun setupRecycler() {
        binding.apply {
            itemListRecycler.adapter = itemAdapter
            queriesRecycler.adapter = queryAdapter
        }
    }

    private fun setupOnClickListener() {
        binding.searchEditText.setOnEditorActionListener { textView, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                requestInProgress()
                viewModel.searchQuery(textView.text.toString())
                hideKeyboard()
                cardQueriesHide()
                return@setOnEditorActionListener true
            }
            return@setOnEditorActionListener false
        }
    }

    private fun setupObservable() {
        viewModel.itemList.observe(viewLifecycleOwner, { itemList ->
            itemAdapter.submitList(itemList)
            requestFinished()
        })
        viewModel.queryList.observe(viewLifecycleOwner, { queryList ->
            queryAdapter.submitList(queryList)
        })
    }

    private fun setupTextWatcher(){
        binding.searchEditText.addTextChangedListener(object : TextWatcher{

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (!p0.isNullOrEmpty()){
                    viewModel.getSaveQueries()
                    cardQueriesShow()
                }else{
                    cardQueriesHide()
                }
            }

            override fun afterTextChanged(p0: Editable?) {}
        })
    }

    private fun hideKeyboard(){
        activity?.let {
            val inputManager = it.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view?.windowToken,0)
        }
    }

    private fun requestInProgress(){
        binding.apply {
            itemListRecycler.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
        }
    }

    private fun requestFinished(){
        binding.apply {
            itemListRecycler.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
    }

    private fun cardQueriesShow(){
        binding.cardQueries.visibility = View.VISIBLE
    }

    private fun cardQueriesHide(){
        binding.cardQueries.visibility = View.GONE
    }

    companion object {
        fun newInstance() = MainFragment()
    }

}